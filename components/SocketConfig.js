const express = require('express');
const http = require('http');
const socketIO = require('socket.io');


const port = 3000;
var app = express();
var server = http.createServer(app);
var io = socketIO(server, {
    path: '/test',
    serveClient: false,
    // below are engine.IO options
    pingInterval: 10000,
    pingTimeout: 5000,
    cookie: false
  });
var messages = []
server.listen(port, () => {
    console.log(port, 'is up');
});

io.on('connection', (socket) => {
    console.log('new user connected');

    socket.on('disconnect', () => {
        console.log('User was disconnected');
    });
    socket.on('onSend', (data) => {
        console.log(data)
        messages.unshift(data[0])
        console.log(messages)
        io.emit('onSend',{message:messages})
    });
});

