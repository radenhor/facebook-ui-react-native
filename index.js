/**
 * @format
 */

import {AppRegistry} from 'react-native';
import App from './App';
import React from 'react'
import {name as appName} from './app.json';
import {createStore,applyMiddleware,compose} from 'redux';
import thunk from 'redux-thunk';
import {Provider} from 'react-redux';
import {chatReducer} from './reducer/chatReducer';
const store = createStore(chatReducer,compose(applyMiddleware(thunk)))
const RNRedux = () => (
    <Provider store = { store }>
      <App />
    </Provider>
  )
  
  AppRegistry.registerComponent(appName, () => RNRedux);
