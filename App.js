/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  Button,
  Image,
} from 'react-native';

import {
  Header,
  LearnMoreLinks,
  Colors,
  DebugInstructions,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';
import React, { Component } from 'react'
import {createAppContainer} from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import Chat from './components/Chat'; 
import Save from './components/Save';
import Feed from './components/Feed';
import Test from './components/Test';
import Icon from 'react-native-vector-icons/AntDesign';
import Icon1 from 'react-native-vector-icons/FontAwesome';
import Icon2 from 'react-native-vector-icons/Entypo';
import Icon3 from 'react-native-vector-icons/Feather';
import { createBottomTabNavigator,createMaterialTopTabNavigator } from 'react-navigation-tabs';
import { createDrawerNavigator } from 'react-navigation-drawer';  


const appNavigation = createBottomTabNavigator({
  Feed : {
    screen : Feed,
    navigationOptions: {
      tabBarLabel: 'Feed',
      tabBarIcon: ({tintColor, activeTintColor}) => (
         <Icon2 name="browser" size={30} color={tintColor} />
         )
    },
  },
  Chat : {
    screen : Chat,
    navigationOptions: {
      tabBarLabel: 'Chat',
      tabBarIcon: ({tintColor, activeTintColor}) => (
         <Icon2 name="chat" size={30} color={tintColor} />
         )
    },
  },
  Save : {
    screen : Save,
    navigationOptions: {
      tabBarLabel: 'Save',
      tabBarIcon: ({tintColor, activeTintColor}) => (
         <Icon2 name="bookmarks" size={30} color={tintColor} />
         )
    },
  },
  Test : {
    screen : Test,
    path: 'test/:user',
    navigationOptions: {
      tabBarLabel: 'Test',
      tabBarIcon: ({tintColor, activeTintColor}) => (
         <Icon2 name="bookmarks" size={30} color={tintColor} />
         )
    },
  }
})
export default createAppContainer(appNavigation);